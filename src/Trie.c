#include "Trie.h"

#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <ctype.h>

#define EDGE_COUNT 28 // only index lowercase a-z, and ' and -

static void AddWordToEdge(Trie *edge, size_t depth, const char *word, void *userData);
static void GetWordsAndUserDataFromEdge(Trie *edge, char *prefix, char** wordArray, void** userDataArray, int *index, size_t maxCount);
static size_t EdgeIndexForChar(char character);
static char CharForEdgeIndex(size_t index);

struct Trie {
    int wordCount;
    int prefixCount;
    void *userData;
    struct Trie *edges[EDGE_COUNT]; 
};


struct LookupResult {
    char *query;
    size_t count;
    Trie *trie;
    size_t retrievedCount;
    char **matches;
    void **userData;
};

Trie* TrieAllocate() {
    Trie *trie = (Trie*) malloc(sizeof(Trie));
    memset(trie,0, sizeof(Trie));
    return trie;
}

void TrieRelease(Trie *trie) {
    if (trie != NULL) {
        for (int i=0; i < EDGE_COUNT ; ++i) {
            Trie *edge = trie->edges[i];
            if ( edge ) {
                TrieRelease(edge);
            }
        }
        free(trie);
    }
}

int TrieAddWord(Trie *trie, const char *word, void* userData) {
    assert(trie != NULL);
    // Check for existing entry
    LookupResult *r = TrieExactLookup(trie, word);
    size_t count = TrieResultGetMatchCount(r);
    TrieReleaseResult(r);
    if ( count == 0 ) {
        AddWordToEdge(trie, 0, word, userData);
        return 0;
    }
    return -1;
}

size_t TrieGetEntryCount(Trie *trie) {
    return trie->prefixCount;
}

size_t EdgeIndexForChar(char character) {
    size_t index = EDGE_COUNT;
    switch (character) {
        case '\'':
            index = 26;
            break;
        case '-':
            index = 27;
            break;
        default: {
            index = tolower(character) - 'a';
        }
    }
    assert(index < EDGE_COUNT);
    return index;
}

char CharForEdgeIndex(size_t index) {
    assert(index < EDGE_COUNT);
    switch (index) {
        case 26: return '\'';
        case 27: return '-';
        default: return 'a' + index;

    }
}

void AddWordToEdge(Trie *edge, size_t depth, const char *word, void *userData) {
    char edgeChar = word[depth];
    if( edgeChar == 0 ) {
        assert(edge->wordCount == 0);
        edge->wordCount++;
        edge->userData = userData;
        return;
    }
    
    edge->prefixCount++;
    size_t edgeIndex = EdgeIndexForChar(edgeChar);

    if( edge->edges[edgeIndex] == NULL ) {
        edge->edges[edgeIndex] = TrieAllocate();
        assert( edge->edges[edgeIndex] != NULL );
    }
    
    AddWordToEdge(edge->edges[edgeIndex], depth+1, word, userData);
}


LookupResult* TrieExactLookup(Trie *trie, const char *query) {
    LookupResult *result = (LookupResult*)malloc(sizeof(LookupResult));
    memset(result, 0, sizeof(LookupResult));
    result->query = malloc(strlen(query)+1);
    strcpy(result->query, query);
    
    Trie* edge = trie;
    for (size_t i = 0; i < strlen(query); ++i ) {
        if ( (edge = edge->edges[EdgeIndexForChar(query[i])]) == NULL ) {
            break;
        }
    }
    if (edge != NULL && edge->wordCount == 1) {
        result->count = 1;
        result->trie = edge;
    } else {
        result->count = 0;
        result->trie = NULL;
    }
    return result;
}

LookupResult* TriePrefixLookup(Trie *trie, const char *query) {
    LookupResult *result = (LookupResult*)malloc(sizeof(LookupResult));
    memset(result, 0, sizeof(LookupResult));
    result->query = malloc(strlen(query)+1);
    strcpy(result->query, query);
    
    Trie* edge = trie;
    for (size_t i = 0; i < strlen(query); ++i ) {
        if ( (edge = edge->edges[EdgeIndexForChar(query[i])]) == NULL ) {
            break;
        }
    }
    if (edge == NULL) {
        result->count = 0;
        result->trie = NULL;
    } else {
        result->count = edge->wordCount + edge->prefixCount;
        result->trie = edge;
    }
    return result;
}

void GetWordsAndUserDataFromEdge(Trie *edge, char *prefix, char** wordArray, void** userDataArray, int *index, size_t maxCount) {
    if ( (*index) >= maxCount ) {
        return;
    }
    if (edge->wordCount > 0) {
        size_t prefixLength = strlen(prefix);
        wordArray[*index] = (char*)malloc(prefixLength + 1);
        memcpy(wordArray[*index], prefix, prefixLength + 1);
        wordArray[*index][prefixLength] = 0;
        
        if ( edge->userData != NULL ) {
            userDataArray[*index] = edge->userData;
        }
        
        (*index)++;
    }
    for (size_t i = 0; i < EDGE_COUNT && (*index) < maxCount; ++i) {
        
        Trie *currentEdge = edge->edges[i];
        
        if ( currentEdge != NULL ) {
            size_t currentPrefixLength = strlen(prefix) + 1;
            char currentPrefix[currentPrefixLength+1];
            memcpy(currentPrefix, prefix, currentPrefixLength-1);
            currentPrefix[currentPrefixLength-1] = CharForEdgeIndex(i);
            currentPrefix[currentPrefixLength] = 0;
            
            GetWordsAndUserDataFromEdge(currentEdge, currentPrefix, wordArray, userDataArray, index, maxCount);
        }
        
        
    }
}

size_t TrieResultGetMatchCount(LookupResult* result) {
    return result->count;
}

size_t TrieResultGetMatches(LookupResult* result, char*** wordArray, void*** userDataArray, size_t maxCount) {
    int index = 0;
    if ( result->retrievedCount > 0 ) {
        printf("%s can only be called once, returning 0 results.\n",__PRETTY_FUNCTION__);
        wordArray = NULL;
        userDataArray = NULL;
        return 0;
    }
    
    result->retrievedCount = result->count;
    if ( result->retrievedCount > maxCount ) {
        result->retrievedCount = maxCount;
    }
    if (result->retrievedCount == 0) {
        result->matches = NULL;
        result->userData = NULL;
        return 0;
    }
    
    result->matches = (char**)malloc(result->retrievedCount * sizeof(char*));
    result->userData = (void**)malloc(result->retrievedCount * sizeof(void*));
    
    GetWordsAndUserDataFromEdge(result->trie, result->query, result->matches, result->userData, &index, maxCount);
    
    assert(index == result->retrievedCount || index == maxCount);
    if (wordArray != NULL) {
        *wordArray = result->matches;
    }
    if (userDataArray != NULL) {
        *userDataArray = result->userData;
    }
    
    return result->retrievedCount;
}

void TrieReleaseResult(LookupResult* result) {
    free(result->query);
    for (size_t i = 0; i < result->retrievedCount; ++i) {
        free(result->matches[i]);
    }
    free(result->matches);
    free(result->userData);
    free(result);
}


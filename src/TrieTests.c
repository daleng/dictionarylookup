#include <stdio.h>
#include <assert.h>
#include <string.h>

#include "Trie.h"

static Trie* FillTestTrie();
static void TestPrefixLookup();
static void TestWordLookup();
static void TestExactLookup();
static void TestEmptyExactLookup();
static void TestEmptyPrefixLookup();
static void TestNonexistentPrefixLookup();
static void TestNonexistentExactLookup();


void RunTrieTests() {
    TestPrefixLookup();
    TestWordLookup();
    TestExactLookup();
    TestEmptyExactLookup();
    TestEmptyPrefixLookup();
    TestNonexistentPrefixLookup();
    TestNonexistentExactLookup();
}

Trie* FillTestTrie() {
    Trie *testTrie = TrieAllocate();
    const char *words[] = {
        "romane",
        "romanus",
        "romulus",
        "rubens",
        "ruber",
        "rubicon",
        "rubicundus"
    };
    size_t wordCount = sizeof(words) / sizeof(char*);
    for (int i = 0; i < wordCount; ++i) {
        TrieAddWord(testTrie, words[i], (void*)words[i]); // Use word as userdata also
    }
    return testTrie;
}

void TestPrefixLookup() {
    Trie *trie = FillTestTrie();
    LookupResult *r = TriePrefixLookup(trie, "ro");
    char** results = NULL;
    void** userData = NULL;
    TrieResultGetMatches(r, &results, &userData, TrieResultGetMatchCount(r));

    assert(TrieResultGetMatchCount(r) == 3);
    assert(strcmp(results[0], "romane") == 0);
    assert(strcmp(results[1], "romanus") == 0);
    assert(strcmp(results[2], "romulus") == 0);
    assert(strcmp(userData[0], "romane") == 0);
    assert(strcmp(userData[1], "romanus") == 0);
    assert(strcmp(userData[2], "romulus") == 0);
    
    TrieReleaseResult(r);
    TrieRelease(trie);
}

void TestWordLookup() {
    Trie *trie = FillTestTrie();
    LookupResult *r = TriePrefixLookup(trie, "rubicundus");
    char** results = NULL;
    void** userData = NULL;
    TrieResultGetMatches(r, &results, &userData, TrieResultGetMatchCount(r));

    assert(TrieResultGetMatchCount(r) == 1);
    assert(strcmp(results[0], "rubicundus") == 0);
    assert(strcmp(userData[0], "rubicundus") == 0);
    TrieRelease(trie);
}

void TestExactLookup() {
    Trie *trie = FillTestTrie();
    
    LookupResult *r = TrieExactLookup(trie, "romane");
    char** results = NULL;
    void** userData = NULL;
    TrieResultGetMatches(r, &results, &userData, TrieResultGetMatchCount(r));
    
    assert(TrieResultGetMatchCount(r) == 1);
    assert(strcmp(results[0], "romane") == 0);
    assert(strcmp(userData[0], "romane") == 0);
    
    TrieReleaseResult(r);
    TrieRelease(trie);
}

void TestEmptyExactLookup() {
    Trie *trie = FillTestTrie();
    
    LookupResult *r = TrieExactLookup(trie, "");
    assert(TrieResultGetMatchCount(r) == 0);
    TrieReleaseResult(r);
    
    TrieRelease(trie);
}

void TestEmptyPrefixLookup() {
    Trie *trie = FillTestTrie();
    
    LookupResult *r = TriePrefixLookup(trie, "");
    char** results = NULL;
    void** userData = NULL;
    TrieResultGetMatches(r, &results, &userData, TrieResultGetMatchCount(r));
    
    assert(TrieResultGetMatchCount(r) == 7);
    assert(strcmp(results[0], "romane") == 0);
    assert(strcmp(results[1], "romanus") == 0);
    assert(strcmp(results[2], "romulus") == 0);
    assert(strcmp(results[3], "rubens") == 0);
    assert(strcmp(results[4], "ruber") == 0);
    assert(strcmp(results[5], "rubicon") == 0);
    assert(strcmp(results[6], "rubicundus") == 0);
    assert(strcmp((char*)userData[0], "romane") == 0);
    assert(strcmp((char*)userData[1], "romanus") == 0);
    assert(strcmp((char*)userData[2], "romulus") == 0);
    assert(strcmp((char*)userData[3], "rubens") == 0);
    assert(strcmp((char*)userData[4], "ruber") == 0);
    assert(strcmp((char*)userData[5], "rubicon") == 0);
    assert(strcmp((char*)userData[6], "rubicundus") == 0);
    
    TrieReleaseResult(r);
    TrieRelease(trie);
}


void TestNonexistentPrefixLookup() {
    Trie *trie = FillTestTrie();
    
    LookupResult *r = TriePrefixLookup(trie, "ra");
    assert(TrieResultGetMatchCount(r) == 0);
    TrieReleaseResult(r);
    
    TrieRelease(trie);
}

void TestNonexistentExactLookup() {
    Trie *trie = FillTestTrie();
    
    LookupResult *r = TrieExactLookup(trie, "rubicundes");
    assert(TrieResultGetMatchCount(r) == 0);
    TrieReleaseResult(r);
    
    TrieRelease(trie);
}

//
//  main.c
//  DictonaryLookup
//
//  Created by Johannes Alming Daleng on 8/17/13.
//  Copyright (c) 2013 JAD. All rights reserved.
//

#include <stdio.h>
#include <string.h>
#include <errno.h>

#include "TrieTest.h"
#include "Dictionary.h"

int main(int argc, const char * argv[]) {
    const char *dictionaryDirPath = "./wordlists";
    Dictionary *dictionary = NULL;

    if ( argc == 2 && strcmp(argv[1], "--test") == 0) {
        printf("Running tests\n");
        RunTrieTests();
        printf("Tests done\n");
        return 0;
    } else if ( argc == 1 ) {
        printf("No path argument, trying default dictionary location.\n");
        dictionaryDirPath = "./wordlists";
    } else if ( argc == 2 ) {
        dictionaryDirPath = argv[1];
    } else {
        goto PrintUsage;
    }
    
    dictionary = DictionaryNewFromFiles(dictionaryDirPath);
    
    if ( dictionary == NULL ) {
        printf("Failed to open dictionary from '%s': %s.\n",dictionaryDirPath, strerror(errno));
        goto PrintUsage;
    }
    
    DictionaryRunCli(dictionary);
    
    DictionaryRelease(dictionary);
    return 0;
    
PrintUsage:
    printf("\n\tUsage: DictionaryLookup [dictionaryDir]\n");
    return 1;
}


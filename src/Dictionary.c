//
//  Dictionary.c
//  DictonaryLookup
//
//  Created by Johannes Alming Daleng on 8/18/13.
//  Copyright (c) 2013 JAD. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>
#include <errno.h>
#include <assert.h>
#include <ctype.h>

#include "Dictionary.h"
#include "Trie.h"

typedef struct DictionaryEntry DictionaryEntry;

struct Dictionary {
    Trie *trie;
    
    char **languages;
    size_t languageCount;
    size_t languageBufSize;
    
    char **categories;
    size_t categoryCount;
    size_t categoryBufSize;
    
    DictionaryEntry *firstEntry;
    DictionaryEntry *lastEntry;
    
    size_t wordCount;
};

struct DictionaryEntry {
    char *word;
    const char *language;
    const char *category;
    DictionaryEntry *next;
};

static void LoadDictionaryFile(Dictionary *dictionary, const char* containingDirectory, const char* file);
static void ParseAndResolveCategoryAndLanguage(Dictionary *dictionary, const char* file, char** categoryPtr, char** languagePtr);
static char* FindOrAddListEntry(char***list, size_t *count, size_t *bufSize, char *entry);
static int ReadLine(FILE* file, char* lineBuffer, size_t lineBufferSize);
static DictionaryEntry* NewDictionaryEntry(Dictionary *dictionary, const char *word);
static int WordIsAscii(const char *word);
static void PrintHelp();

Dictionary* DictionaryNewFromFiles(const char* containingDirectory) {
    printf("Loading dictionary from '%s' ...\n",containingDirectory);
    
    Dictionary *dictionary;
    struct stat statbuf;
    DIR *dir;
    struct dirent *direntry;
    
    // Check that path points to a directory
    if ( !(stat(containingDirectory, &statbuf) == 0 && S_ISDIR(statbuf.st_mode)) ) {
        errno = ENOENT;
        return NULL;
    }
    
    if( (dir = opendir(containingDirectory)) == NULL) {
        return NULL;
    }
    
    dictionary = (Dictionary*)malloc(sizeof(Dictionary));
    memset(dictionary, 0, sizeof(Dictionary));
    dictionary->trie = TrieAllocate();
    
    // Load all files in directory
    while ( (direntry = readdir(dir)) != NULL ) {
        if ( direntry->d_name[0] == '.' ) {
            continue; // Skip '.', '..' and hidden files
        }
        if ( direntry->d_type != DT_REG ) {
            continue; // Skip non-file entries
        }
        
        // Load words from entry
        LoadDictionaryFile(dictionary, containingDirectory, direntry->d_name);
    }
    closedir(dir);

    // Sanity check
    assert(dictionary->wordCount == TrieGetEntryCount(dictionary->trie));
    
    return dictionary;
}

void DictionaryRunCli(Dictionary *dictionary) {
    printf("Welcome to this commandline dictionary.");
    printf(" A dictionary of %zd words is currently loaded.\n\n", dictionary->wordCount);
    
    PrintHelp();
    
    char line[1024];
    line[0] = 0;
    printf("> ");
    while ( ReadLine(stdin, line, sizeof(line)) == 0 ) {
        if ( strcmp(line, "exit") == 0 ) {
            break;
        } else if ( strcmp(line, "help") == 0 ) {
            PrintHelp();
        } else if ( memcmp(line, "pre", 3) == 0 ) {
            if ( strlen(line) > 4 ) {
                char *query = line+4;
                int queryValid = 1;
                for (int i=0; i < strlen(query); ++i) {
                    int asciiIndex = tolower(query[i]) - 'a';
                    if ( (asciiIndex < 0 || asciiIndex >= 26) && ! ( query[i] == '-' || query[i] == '\'' )) {
                        queryValid = 0;
                    }
                }
                if ( queryValid ) {
                    LookupResult *lr = TriePrefixLookup(dictionary->trie, query);
                    DictionaryEntry **entries = NULL;
                    TrieResultGetMatches(lr, NULL, (void***)&entries, 100);
                    size_t count = TrieResultGetMatchCount(lr);
                    if ( count > 100 ) {
                        printf("There are %zd words beginning with '%s': The first 100 are:\n", count, query);
                    } else {
                        printf("There are %zd words beginning with '%s':\n", count, query);
                    }
                    for (size_t i = 0; i < count && i < 100; ++i) {
                        printf("%s ", entries[i]->word);
                    }
                    printf("\n");
                    TrieReleaseResult(lr);
                } else {
                    printf("Only characters A-Z, a-z, - and ' are allowed in query.\n");
                }
            } else {
                printf("Missing argument to command 'pre'\n");
            }
        } else if ( memcmp(line, "exact", 5) == 0 ) {
            if ( strlen(line) > 6 ) {
                char *query = line+6;
                int queryValid = 1;
                for (int i=0; i < strlen(query); ++i) {
                    int asciiIndex = tolower(query[i]) - 'a';
                    if ( (asciiIndex < 0 || asciiIndex >= 26) && ! ( query[i] == '-' || query[i] == '\'' )) {
                        queryValid = 0;
                    }
                }
                if ( queryValid ) {
                    LookupResult *lr = TrieExactLookup(dictionary->trie, query);
                    size_t count = TrieResultGetMatchCount(lr);
                    if ( count > 0 ) {
                        DictionaryEntry **entries = NULL;
                        TrieResultGetMatches(lr, NULL, (void***)&entries, 1);
                        printf("The word '%s' was found in category '%s' of language '%s'\n", entries[0]->word, entries[0]->category, entries[0]->language);
                    } else {
                        printf("'%s' was not found in the dictionary.\n", query);
                    }
                    TrieReleaseResult(lr);
                } else {
                    printf("Only characters A-Z, a-z, - and ' are allowed in query.\n");
                }
            } else {
                printf("Missing argument to command 'exact'\n");
            }
        } else {
            printf("Unknown command\n");
        }
        printf("> ");
    }
    
    printf("bye\n");
}


void PrintHelp() {
    printf("Available commands are:\n");
    printf("  pre    Prefix lookup, finds words beginning with the given prefix.\n");
    printf("  exact  Exact word lookup, checks for the given word in the dictionary.\n");
    printf("  help   Displays this help message.\n");
    printf("  exit   Exits the dictionary.\n");
}

void LoadDictionaryFile(Dictionary *dictionary, const char* containingDirectory, const char* file) {
    size_t containingPathLen = strlen(containingDirectory);
    size_t fileNameLen = strlen(file);

    char *fileCategory = NULL;
    char *fileLanguage = NULL;
    ParseAndResolveCategoryAndLanguage(dictionary, file, &fileCategory, &fileLanguage);

    
    // Construct path to entry
    char filepath[containingPathLen+fileNameLen+2];
    
    memcpy(filepath, containingDirectory, containingPathLen);
    filepath[containingPathLen] = '/';
    memcpy(filepath+(containingPathLen+1), file, fileNameLen);
    filepath[containingPathLen+1+fileNameLen] = 0;
    
    FILE *f = fopen(filepath, "r");
    const int LINE_BUFFER_SIZE=256;
    char line[LINE_BUFFER_SIZE];
    while ( ReadLine(f, line, LINE_BUFFER_SIZE)==0 ) {
        if ( WordIsAscii(line) ) {
            DictionaryEntry *entry = NewDictionaryEntry(dictionary, line);
            entry->category = fileCategory;
            entry->language = fileLanguage;
            if ( TrieAddWord(dictionary->trie, line, entry) == 0 ) {
                dictionary->wordCount++;
            }
        }
    }
    fclose(f);

}

void ParseAndResolveCategoryAndLanguage(Dictionary *dictionary, const char* file, char** categoryPtr, char** languagePtr) {
    int posOfFirstDash=-1;
    
    // Find category and language from file name
    for (int i = 0; i < (int)strlen(file); ++i) {
        if ( file[i] == '-' ) {
            posOfFirstDash = i;
            break;
        }
    }
    assert( posOfFirstDash > 0 );
    
    size_t categoryEndIndex = strlen(file) - 3;
    size_t categoryStartIndex = posOfFirstDash+1;
    size_t languageEndIndex = posOfFirstDash;
    
    size_t categoryLen = categoryEndIndex-categoryStartIndex;
    char category[categoryLen+1];
    memcpy(category, file+categoryStartIndex, categoryLen);
    category[categoryEndIndex-categoryStartIndex] = 0;
    
    char language[languageEndIndex+1];
    memcpy(language, file, languageEndIndex);
    language[languageEndIndex] = 0;
    
    // Find or add category to dictionary
    *categoryPtr = FindOrAddListEntry(&dictionary->categories, &dictionary->categoryCount, &dictionary->categoryBufSize, category);
    *languagePtr = FindOrAddListEntry(&dictionary->languages, &dictionary->languageCount, &dictionary->languageBufSize, language);
    
}

int ReadLine(FILE* file, char* lineBuffer, size_t lineBufferSize) {
    for (int i=0; i<lineBufferSize; ++i) {
        char c = fgetc(file);
        if ( c == '\n' || c == EOF ) {
            if ( c == EOF && errno == EINTR ) {
                continue;
            }
            lineBuffer[i] = 0;
            return c == EOF ? EOF : 0;
        }
        lineBuffer[i] = c;
    }
    return EOVERFLOW;
}

DictionaryEntry* NewDictionaryEntry(Dictionary *dictionary, const char *word) {
    DictionaryEntry *entry = (DictionaryEntry*)malloc(sizeof(DictionaryEntry));
    memset(entry, 0, sizeof(DictionaryEntry));
    entry->word = malloc(strlen(word) + 1);
    strcpy(entry->word, word);
    
    // Chain entries into a linked list, to facilitate deallocation.
    if (dictionary->firstEntry == NULL) {
        dictionary->firstEntry = entry;
        dictionary->lastEntry = entry;
    } else {
        dictionary->lastEntry->next = entry;
        dictionary->lastEntry = entry;
    }
    return entry;
}

int WordIsAscii(const char *word) {
    const char *c = word;
    while ( *c != 0 ) {
        if ( !isascii(*c) ) return 0;
        ++c;
    }
    return 1;
}

char* FindOrAddListEntry(char***list, size_t *count, size_t *bufSize, char *entry) {
    if ( *bufSize == 0 ) {
        // First entry
        *bufSize = 8;
        *count = 1;
        *list = (char**)malloc(sizeof(char*) * (*bufSize));
        **list = (char*)malloc(strlen(entry)+1);
        strcpy(**list, entry);
        return **list;
    }
    // Look for existing entry
    for (int i=0; i < *count; ++i) {
        if ( strcmp((*list)[i],entry) == 0 ) {
            return (*list)[i];
        }
    }
    // Entry needs to be added
    if ( *count >= *bufSize  ) {
        // Grow list buffer
        *bufSize *= 2;
        *list = realloc(*list, sizeof(char*) * (*bufSize));
    }
    (*list)[*count] = (char*)malloc(strlen(entry)+1);
    strcpy((*list)[*count], entry);
    (*count)++;
    return (*list)[*count-1];
}

void DictionaryRelease(Dictionary* dictionary) {
    if (dictionary != NULL) {
        TrieRelease(dictionary->trie);
        
        // Release entry objects
        DictionaryEntry *entry = dictionary->firstEntry;
        do {
            free(entry->word);
            DictionaryEntry *next = entry->next;
            free(entry);
            entry = next;
        } while ( entry != NULL );
        
        // Release category strings
        for (int i=0; i < dictionary->categoryCount; ++i) {
            free(dictionary->categories[i]);
        }
        free(dictionary->categories);

        // Release language strings
        for (int i=0; i < dictionary->languageCount; ++i) {
            free(dictionary->languages[i]);
        }
        free(dictionary->languages);

        free(dictionary);
    }
}
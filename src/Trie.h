#ifndef DictonaryLookup_Trie_h
#define DictonaryLookup_Trie_h

#include <stdint.h>
#include <stdio.h>

/**
 * @file   Trie.h
 * @Author Johannes A. Daleng (johannes@analogsim.net)
 * @date   August, 2013
 * @brief  Trie data structure.
 *
 * A simple trie implementation.
 *
 * @see: http://en.wikipedia.org/wiki/Trie
 */


/**
 * @brief Trie opaque type
 */
typedef struct Trie Trie;

/**
 * @brief LookupResult opaque type
 */

typedef struct LookupResult LookupResult;

/**
 * @brief Allocate a new trie
 *
 * Must be deallocated using TrieRelease()
 */
Trie* TrieAllocate();

/**
 * @brief Deallocate a trie;
 */
void TrieRelease(Trie *trie);

/**
 * @brief Add a word to the trie
 *
 * Add a word and an optional user data object to the trie. 
 * Storage and lookup in the trie is case insensitive, so if
 * case needs to be preserved, use userData to point to a 
 * case-preserved version of the word.
 * 
 * Only characters a-z, A-Z, ' and - is allowed.
 *
 * The trie does not assume ownership of the userData object.
 *
 * @param word      A word to insert in the trie.
 * @param userData  A user data ptr to associate with the word.
 *
 * @return 0 if the word was added successfully. Return -1 if the word already existed. (perhaps with different case)
 */
int TrieAddWord(Trie *trie, const char *word, void* userData);

/**
 * @brief Get number of totale entries in trie
 *
 * @return The number of entries in the trie, i.e the number of prefixes on the root.
 */
size_t TrieGetEntryCount(Trie *trie);

/**
 * @brief Perform prefix lookup
 *
 * This will return all entries in the trie that has the query as prefix. 
 * The lookup have complexity O(m), where m is the length of the query string.
 * 
 * The caller must deallocate the returned LookupResult by calling TrieReleaseResult().
 *
 * @param trie  A pointer to the trie data structure
 * @param query The prefix string to search for.
 *
 * @return A LookupResult pointer for use with TrieResultGetMatchCount() and TrieResultGetMatches(). 
 */
LookupResult* TriePrefixLookup(Trie *trie, const char *query);

/**
 * @brief Perform exact match lookup
 *
 * @param trie  A pointer to the trie data structure
 * @param query The string to search for.
 *
 * @return A LookupResult pointer for use with TrieResultGetMatchCount() and TrieResultGetMatches().
 *         The result will have either 1 or 0 matches.
 *         The caller must dispose of the returned LookupResult by calling TrieReleaseResult().
 */
LookupResult* TrieExactLookup(Trie *trie, const char *query);

/**
 * @brief Get number of matches returned by lookup query
 *
 * @param result  A pointer to the LookupResult as returned by TriePrefixLookup or TrieExactLookup
 *
 * @return The number of entries in the trie that matched the lookup
 */
size_t TrieResultGetMatchCount(LookupResult* result);

/**
 * @brief Get the matches returned by lookup query
 *
 * @param result        A pointer to the LookupResult as returned by TriePrefixLookup or TrieExactLookup.
 * @param wordArray     A pointer to a char** array that upon return will contain the word matches.
 * @param userDataArray A pointer to a void** array that upon return will contain the user data of the matches.
 * @param maxCount      The max number of matches to return.
 *
 * The strings in wordArray will remain valid until TrieReleaseResult() is called.
 *
 * @return The number of results returned. This willl be the largest of maxCount 
 *         and the number returned by TrieResultGetMatchCount().
 */
size_t TrieResultGetMatches(LookupResult* result, char*** wordArray, void*** userDataArray, size_t maxCount);

/**
 * @brief Release a lookup result.
 */
void TrieReleaseResult(LookupResult* result);

#endif

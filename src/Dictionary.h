
//
//  Dictionary.h
//  DictonaryLookup
//
//  Created by Johannes Alming Daleng on 8/18/13.
//  Copyright (c) 2013 JAD. All rights reserved.
//

#ifndef DictonaryLookup_Dictionary_h
#define DictonaryLookup_Dictionary_h

typedef struct Dictionary Dictionary;

Dictionary* DictionaryNewFromFiles(const char* containingDirectory);
void DictionaryRelease(Dictionary* dictionary);

void DictionaryRunCli(Dictionary* dictionary);

#endif

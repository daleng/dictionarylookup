#
# 'make'        build executable file 'mycc'
# 'make clean'  removes all .o and executable files
#

CC = gcc
CFLAGS = -std=gnu99 -Wall -g
INCLUDES = -I./src
LFLAGS = 

#LIBS = -lmylib -lm
LIBS = 

# define the C source files
SRCS = src/Dictionary.c  src/main.c  src/Trie.c  src/TrieTests.c

OBJS = $(SRCS:.c=.o)

MAIN = dictionary

.PHONY: depend clean

all:	$(MAIN)
	@echo  Built executable: $(MAIN)

$(MAIN): $(OBJS) 
	$(CC) $(CFLAGS) $(INCLUDES) -o $(MAIN) $(OBJS) $(LFLAGS) $(LIBS)

.c.o:
	$(CC) $(CFLAGS) $(INCLUDES) -c $<  -o $@

clean:
	$(RM) src/*.o src/*~ *~ $(MAIN)

# End


Johannes simple dictionary lookup program
=========================================

== Quickstart ==
> make
> ./dictionary

For Mac OS X, there is also a .xcodeproj for use with Xcode.

== Dependencies ==
 * Standard C99 environment. (with gnu extensions to dirent.h)
 * Tested on Ubuntu 12.04 LTS and OS X 10.8.4, but should work mostly anywhere.

== What and why ==
I wanted to implement a data-structure, and chose to implement a trie, (also known as 
a prefix tree or radix tree).  See: http://en.wikipedia.org/wiki/Trie from more info.

To put my trie implementation to a test, I downloaded the largest english dictionary i could 
find, and made a simple command line lookup interface to it.

The most prominent characteristic of a trie is that lookup is O(m) where m is the 
length of the lookup key. Compared to, say a BST with O(log n) complexity, a trie
have the potential to be significantly faster for a large number of entries.

The implementation is quite basic. Each node in supports only characters A-Z, a-z, dash and 
apostrophe (same as single quote in ascii). The trie discards case information. 
Dictionary words containing other characters are ignored. Duplicates, real or because of 
case-insensitivity are ignored.

== Supplied dictionary data ==
Supplied is the SCOWL word list found at http://wordlist.sourceforge.net/
Credit to Kevin Atkinson (kevina at gnu org) for this data.

Even after ignoring the duplicates and unsupported characters, this dataset allowed me to 
test the trie implementation with over 600 000 entries.
